package com.example.livedata.communication

interface RecyclerViewScrollListener {
    fun onScroll(position: Int , title : String?)
}